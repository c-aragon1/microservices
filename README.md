# Microservices

## Proyecto

Los tres proyectos utilizan una base de datos en memoria, y cuentan con archivos `data.sql` para ingresar datos de prueba.

Para compilar los tres proyectos, desde la raiz, ejecutar:

`gradle clean build`

### Catalog

Para ejecutar este subproyecto, despues de compilar, ejecutar con el commando:

`java -jar catalog/build/libs/catalog-0.0.1-SNAPSHOT.jar`

Se ejecuta en http://localhost:8080/catalog/swagger-ui/

### Buyer

Para ejecutar este subproyecto, despues de compilar, ejecutar con el commando:

`java -jar catalog/build/libs/buyer-0.0.1-SNAPSHOT.jar`

Se ejecuta en http://localhost:8081/buyer/swagger-ui/

### Seller (Por agregar al proyecto)

Para ejecutar este subproyecto, despues de compilar, ejecutar con el commando:

`java -jar catalog/build/libs/seller-0.0.1-SNAPSHOT.jar`

Se ejecuta en http://localhost:8082/seller/swagger-ui/


