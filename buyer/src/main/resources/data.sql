insert into invoice (id, date, buyer, tax, total) values (1, '2020-12-12', 'Charly Brow', 48.0, 348.0);

insert into product (id, upc, quantity, price, invoice_id) values ( 1, '000001', 10, 10, 1);
insert into product (id, upc, quantity, price, invoice_id) values ( 2, '000002', 10, 10, 1);
insert into product (id, upc, quantity, price, invoice_id) values ( 3, '000003', 10, 10, 1);

insert into invoice (id, date, buyer, tax, total) values (2, '2020-12-13', 'Charly Brow Segundo', 4.8, 34.8);

insert into product (id, upc, quantity, price, invoice_id) values ( 4, '000003', 1, 10, 2);
insert into product (id, upc, quantity, price, invoice_id) values ( 5, '000004', 1, 10, 2);
insert into product (id, upc, quantity, price, invoice_id) values ( 6, '000005', 1, 10, 2);