package com.tasunsoft.buyer.controller;

import com.tasunsoft.buyer.domain.Invoice;
import com.tasunsoft.buyer.dto.InvoiceDto;
import com.tasunsoft.buyer.mapper.Mapper;
import com.tasunsoft.buyer.repository.InvoiceRepository;
import com.tasunsoft.buyer.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("invoice")
public class InvoiceController {

    private InvoiceService invoiceService;

    private Mapper mapper;

    @Autowired
    public InvoiceController(
            final Mapper mapper,
            final InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
        this.mapper = mapper;
    }

    @PostMapping
    public InvoiceDto buy(final @RequestBody InvoiceDto invoiceDto) {
        Invoice invoice = mapper.invoiceDto2Invoice(invoiceDto);
        return mapper.invoice2InvoiceDto(invoiceService.save(invoice));
    }

    @GetMapping(path = "{id}")
    public InvoiceDto get(final @PathVariable Long id) {
        Invoice invoice = invoiceService.get(id);
        return mapper.invoice2InvoiceDto(invoice);
    }
}
