package com.tasunsoft.buyer.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InvoiceDto {

    private Long id;

    private Date date;

    private String buyer;

    private List<ProductDto> products;

    private Double tax;

    private Double total;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public List<ProductDto> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDto> products) {
        this.products = products;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public void addProductDto(final ProductDto productDto) {
        if (products == null) {
            products = new ArrayList<>();
        }
        products.add(productDto);
    }
}
