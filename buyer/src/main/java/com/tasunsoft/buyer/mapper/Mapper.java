package com.tasunsoft.buyer.mapper;

import com.tasunsoft.buyer.domain.Invoice;
import com.tasunsoft.buyer.domain.Product;
import com.tasunsoft.buyer.dto.InvoiceDto;
import com.tasunsoft.buyer.dto.ProductDto;
import com.tasunsoft.common.dto.ProductDto4Catalog;
import org.springframework.stereotype.Component;

@Component
public class Mapper {

    public Invoice invoiceDto2Invoice(final InvoiceDto invoiceDto) {
        Invoice invoice = new Invoice();
        invoice.setId(invoiceDto.getId());
        invoice.setBuyer(invoiceDto.getBuyer());
        invoice.setDate(invoiceDto.getDate());
        invoice.setTax(invoiceDto.getTax());
        invoice.setTotal(invoiceDto.getTotal());
        for(ProductDto productDto : invoiceDto.getProducts()) {
            invoice.addProduct(this.productDto2Product(productDto));
        }
        return invoice;
    }

    public InvoiceDto invoice2InvoiceDto(final Invoice invoice) {
        InvoiceDto invoiceDto = new InvoiceDto();
        invoiceDto.setId(invoice.getId());
        invoiceDto.setDate(invoice.getDate());
        invoiceDto.setTax(invoice.getTax());
        invoiceDto.setBuyer(invoice.getBuyer());
        invoiceDto.setTotal(invoice.getTotal());

        for (Product product : invoice.getProducts()) {
            invoiceDto.addProductDto(this.product2ProductDto(product));
        }

        return invoiceDto;
    }

    public Product productDto2Product(final ProductDto productDto) {
        Product product = new Product();
        product.setPrice(productDto.getPrice());
        product.setUpc(productDto.getUpc());
        product.setQuantity(productDto.getQuantity());
        product.setId(productDto.getId());
        return product;
    }

    public ProductDto product2ProductDto(final Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setUpc(product.getUpc());
        productDto.setQuantity(product.getQuantity());
        productDto.setPrice(product.getPrice());
        return productDto;
    }

    public ProductDto4Catalog product2ProductDto4Catalog(final Product product) {
        ProductDto4Catalog productDto4Catalog = new ProductDto4Catalog();
        productDto4Catalog.setPrice(product.getPrice());
        productDto4Catalog.setUpc(product.getUpc());
        productDto4Catalog.setQuantity(product.getQuantity());
        return productDto4Catalog;
    }
}
