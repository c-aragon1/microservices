package com.tasunsoft.buyer.service;

import com.tasunsoft.buyer.domain.Invoice;
import com.tasunsoft.buyer.domain.Product;
import com.tasunsoft.buyer.mapper.Mapper;
import com.tasunsoft.buyer.repository.InvoiceRepository;
import com.tasunsoft.buyer.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class InvoiceService {

    private InvoiceRepository invoiceRepository;

    private ProductRepository productRepository;

    private RestTemplate restTemplate;

    private Mapper mapper;

    private String url = "http://localhost:8080/catalog/product/received";

    @Autowired
    public InvoiceService(final InvoiceRepository invoiceRepository,
                          final ProductRepository productRepository,
                          final Mapper mapper) {
        this.invoiceRepository = invoiceRepository;
        this.productRepository = productRepository;
        this.mapper = mapper;
        this.restTemplate = new RestTemplate();
    }

    public Invoice save(final Invoice invoice) {
        invoiceRepository.save(invoice);

        for (Product product : invoice.getProducts()) {
            product.setInvoice(invoice);
            restTemplate.postForEntity(url, mapper.product2ProductDto4Catalog(product), Void.class);
        }

        productRepository.saveAll(invoice.getProducts());

        return invoice;

    }

    public Invoice get(final Long invoiceId) {
        Optional<Invoice> invoice = invoiceRepository.findById(invoiceId);
        return invoice.orElseThrow();
    }

}

