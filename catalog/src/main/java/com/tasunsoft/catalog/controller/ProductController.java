package com.tasunsoft.catalog.controller;

import com.tasunsoft.catalog.dto.ProductDto;
import com.tasunsoft.catalog.mapper.Mapper;
import com.tasunsoft.catalog.service.ProductService;
import com.tasunsoft.common.dto.ProductDto4Catalog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("product")
public class ProductController {

    private ProductService productService;

    private Mapper mapper;

    @Autowired
    public ProductController(final ProductService productService,
                             final Mapper mapper) {
        this.productService = productService;
        this.mapper = mapper;
    }

    @PostMapping
    public ProductDto create(final @RequestBody ProductDto productDto) {
        return mapper.product2ProductDto(productService.save(mapper.product2ProductDto(productDto)));
    }

    @PostMapping("received")
    public void received (final @RequestBody ProductDto4Catalog productDto4Catalog) {
        productService.received(productDto4Catalog);
    }

    @GetMapping
    public List<ProductDto> get() {
        return productService.getProducts().stream().map(product -> mapper.product2ProductDto(product)).collect(Collectors.toList());
    }

    @GetMapping(path = "{id}")
    public ProductDto get(final @PathVariable Long id) {
        return mapper.product2ProductDto(productService.getProduct(id));
    }

}
