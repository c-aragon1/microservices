package com.tasunsoft.catalog.repository;

import com.tasunsoft.catalog.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Optional<Product> findByUpc(String upc);
}
