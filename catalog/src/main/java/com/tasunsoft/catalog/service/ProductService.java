package com.tasunsoft.catalog.service;

import com.tasunsoft.catalog.domain.Product;
import com.tasunsoft.catalog.mapper.Mapper;
import com.tasunsoft.catalog.repository.ProductRepository;
import com.tasunsoft.common.dto.ProductDto4Catalog;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private ProductRepository productRepository;

    public ProductService(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product save(final Product product) {
        return productRepository.save(product);
    }

    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    public Product getProduct(final Long id) {
        return productRepository.getOne(id);
    }

    public void received(final ProductDto4Catalog productDto4Catalog) {
        Optional<Product> product = productRepository.findByUpc(productDto4Catalog.getUpc());
        if (product.isPresent()) {
            //Inventory update
            product.get().setInventory(product.get().getInventory() + productDto4Catalog.getQuantity());
            //Inventory price
            product.get().setPrice(product.get().getPrice());
            productRepository.save(product.get());
        }
    }
}
