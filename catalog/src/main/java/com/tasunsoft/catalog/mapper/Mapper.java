package com.tasunsoft.catalog.mapper;

import com.tasunsoft.catalog.domain.Product;
import com.tasunsoft.catalog.dto.ProductDto;
import org.springframework.stereotype.Component;

@Component
public class Mapper {

    public Product product2ProductDto(final ProductDto productDto) {
        Product product = new Product();
        product.setId(productDto.getId());
        product.setName(productDto.getName());
        product.setUpc(productDto.getUpc());
        product.setInventory(productDto.getInventory());
        product.setPrice(productDto.getPrice());
        return product;
    }

    public ProductDto product2ProductDto(final Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setUpc(product.getUpc());
        productDto.setPrice(product.getPrice());
        productDto.setInventory(product.getInventory());
        return productDto;
    }

}
